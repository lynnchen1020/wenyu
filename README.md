# wenyu

> portfolio

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build public & deploy on gitlab
npm run master
```