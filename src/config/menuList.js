export default
[
  {
    "menuName": {
      "url": "sport_industry",
      "tab": "Sport Industry",
      "img": "static/img/corechair_cover_un.png",
      "imgH": "static/img/windgodlion_cover.png",
      "subMenu": [
        {
          "url": "core_chair",
          "tab": "Core Chair",
          "img": "static/img/corechair_cover_un.png",
          "imgH": "static/img/windgodlion_cover.png"
        },
        {
          "url": "dex",
          "tab": "DEX",
          "img": "static/img/dex_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        },
        {
          "url": "m_5",
          "tab": "M-5",
          "img": "static/img/m5_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        },
        {
          "url": "mega_Air",
          "tab": "Mega Air",
          "img": "static/img/megaair_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        },
        {
          "url": "rolloer_ratchet",
          "tab": "Rolloer Ratchet",
          "img": "static/img/ratchet_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        },
        {
          "url": "gun_e_pump",
          "tab": "Gun E Pump",
          "img": "static/img/speaker.jpg",
          "imgH": "static/img/windgodlion_cover.png"
        }
      ]
    }
  },
  {
    "menuName": {
      "url": "engadget",
      "tab": "Engadget",
      "img": "static/img/esport_cover.png",
      "imgH": "static/img/windgodlion_cover.png",
      "subMenu": [
        {
          "url": "eSport",
          "tab": "eSport",
          "img": "static/img/esport_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        },
        {
          "url": "medion_dab_speaker",
          "tab": "Medion DAB Speaker",
          "img": "static/img/medion_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        },
        {
          "url": "duralcell_2_disk",
          "tab": "Duralcell 2\" Disk",
          "img": "static/img/duracell_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        },
        {
          "url": "bonzai",
          "tab": "Bonzai",
          "img": "static/img/bonzai_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        }
      ]
    }
  },
  {
    "menuName": {
      "url": "home_appliance",
      "tab": "Home Appliance",
      "img": "static/img/gameboy.jpg",
      "imgH": "static/img/windgodlion_cover.png",
      "subMenu": [
        {
          "url": "sweeping_robot",
          "tab": "Sweeping Robot",
          "img": "static/img/gameboy.jpg",
          "imgH": "static/img/windgodlion_cover.png"
        },
        {
          "url": "4in1_pot",
          "tab": "4 in 1 Pot",
          "img": "static/img/4in1pot_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        }
      ]
    }
  },
  {
    "menuName": {
      "url": "medical_engadget",
      "tab": "Medical Engadget",
      "img": "static/img/safeplay_cover.png",
      "imgH": "static/img/windgodlion_cover.png",
      "subMenu": [
        {
          "url": "safeplay",
          "tab": "SafePlay",
          "img": "static/img/safeplay_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        },
        {
          "url": "pixotest",
          "tab": "PixoTest",
          "img": "static/img/pixotest_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        }
      ]
    }
  },
  {
    "menuName": {
      "url": "b2b_product",
      "tab": "B2B Product",
      "img": "static/img/argox_cover.png",
      "imgH": "static/img/windgodlion_cover.png",
      "subMenu": [
        {
          "url": "go_light",
          "tab": "Go-Light",
          "img": "static/img/speaker.jpg",
          "imgH": "static/img/windgodlion_cover.png"
        },
        {
          "url": "argox_2_barcode_printer",
          "tab": "Argox 2\" Barcode Printer",
          "img": "static/img/argox_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        }
      ]
    }
  },
  {
    "menuName": {
      "url": "culture_n_creative",
      "tab": "Culture & Creative",
      "img": "static/img/speaker.jpg",
      "imgH": "static/img/windgodlion_cover.png",
      "subMenu": [
        {
          "url": "rii_book",
          "tab": "Rii Book",
          "img": "static/img/rii_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        },
        {
          "url": "wind_lion",
          "tab": "wind Lion",
          "img": "static/img/windgodlion_cover.png",
          "imgH": "static/img/windgodlion_cover.png"
        }
      ]
    }
  }
]