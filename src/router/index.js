import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/home'
import Sport_Industry from '@/components/sport_industry'
import Core_Chair from '@/components/sport_industry/core_chair'
import Dex from '@/components/sport_industry/dex'
import M_5 from '@/components/sport_industry/m_5'
import Engadget from '@/components/engadget'
import Home_Appliance from '@/components/home_appliance'
import Medical_Engadget from '@/components/medical_engadget'
import B2b_Product from '@/components/b2b_product'
import Culture_n_Creative from '@/components/culture_n_creative'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      // name: 'home',
      component: Home
    },
    {
      name: 'sport_industry',
      path: '/sport_industry',
      component: Sport_Industry,
    },
    {
      name: "core_chair",
      path: '/sport_industry/core_chair',
      component: Core_Chair,
    },
    {
      name: "dex",
      path: '/sport_industry/dex',
      component: Dex,
    },
    {
      name: "m_5",
      path: '/sport_industry/m_5',
      component: M_5,
    },
    {
      name: "engadget",
      path: '/engadget',
      component: Engadget,
    },
    {
      name: "home_appliance",
      path: '/home_appliance',
      component: Home_Appliance
    },
    {
      name: "medical_engadget",
      path: '/medical_engadget',
      component: Medical_Engadget
    },
    {
      name: "b2b_product",
      path: '/b2b_product',
      component: B2b_Product
    },
    {
      name: "culture_n_creative",
      path: '/culture_n_creative',
      component: Culture_n_Creative
    }
  ]
})
